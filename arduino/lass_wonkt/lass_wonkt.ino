#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <time.h>

#include <Wire.h>
#include <EEPROM.h>
#include <DHT.h>
#include <SoftwareSerial.h>
#include <PMS.h>
#include <LiquidCrystal_I2C.h>
#include <TinyGPS.h>
//#define CFG_ENABLE_MEMORYFREE 1
#ifdef CFG_ENABLE_MEMORYFREE
#include <MemoryFree.h>
#endif				/* CFG_ENABLE_MEMORYFREE */

#define FPSTR001( pgm_ptr ) ( reinterpret_cast< const __FlashStringHelper * >( pgm_ptr ) )

// defines pins numbers
const int pinDHT11 = A0;
const int pinLED = 13;
const int pinPMS_RX = 3;
const int pinPMS_TX = 2;
const int pinESP_RX = 5;
const int pinESP_TX = 4;
const int pinGPS_RX = 15;
const int pinGPS_TX = 16;

// i2c address
const int i2cAddr_LCD = 0x27;

// peripherals
DHT dht(pinDHT11, DHT11);
SoftwareSerial serialPortPMS(pinPMS_RX, pinPMS_TX);
SoftwareSerial serialPortESP(pinESP_RX, pinESP_TX);
SoftwareSerial serialPortGPS(pinGPS_RX, pinGPS_TX);
PMS pms(serialPortPMS);
LiquidCrystal_I2C lcd(i2cAddr_LCD, 16, 2);
TinyGPS gps;

// EEPROM address
const int eepromAddrFakeGPS = 0;
const int eepromAddrFakeGPSLat = 1;	// 1~4 to store float
const int eepromAddrFakeGPSLon = 5;	// 5~8 to store float
const int eepromAddrTimezoneH = 9;
const int eepromAddrFakeGPSAlt = 10;	// 10~13 to store float

// Global variables
float temperatureHumidValue[2] = {
    0, 0
};

PMS::DATA pmsData;
int8_t espCIPStatus = -1;
unsigned long sntpPrevMS = 0;
unsigned long sntpEpoch = 0;
uint8_t IP_AP[4] = {
    0, 0, 0, 0
};

uint8_t IP_STA[4] = {
    0, 0, 0, 0
};

uint8_t MAC_STA[6] = {
    0, 0, 0, 0, 0, 0
};

void setup()
{
    // Sets the two pins as Outputs
    pinMode(pinLED, OUTPUT);
    Serial.begin(9600);

    while (!Serial) {
	;
    }
    serialPortPMS.begin(9600);
    serialPortESP.begin(9600);
    serialPortGPS.begin(9600);
    serialPortESP.listen();
    //pms.passiveMode();

    dht.begin();

    lcd.init();
    lcd.backlight();

    digitalWrite(pinLED, LOW);
}

/**
 * get difference from two time
 *
 * @param oldTime old time
 * @param newTime new time
 * @return the difference in MS
 */
unsigned long diffMS(unsigned long oldTime, unsigned long newTime)
{
    unsigned long ret;
    if (oldTime <= newTime) {
	return newTime - oldTime;
    }
    ret = (ULONG_MAX - oldTime) + newTime;
    return ret;
}

/**
 * Convert string to float.
 *
 * A simple function to convert a float string (+|-)XXX.XXXX to a float.
 *
 * @param s the string
 * @return float number
 */
float str2float(const char *s)
{
    float sign = 1.0;
    float intPart = 0.0;
    float pFactor = 0.1;
    uint8_t numOfPoint = 0;
    for (const char *c = s; *c != '\0'; c++) {
	if (*c == '-') {
	    sign = -sign;
	} else if ('0' <= *c && *c <= '9') {
	    switch (numOfPoint) {
	    case 0:
		intPart = intPart * 10.0;
		intPart = intPart + ((float) (*c - '0'));
		break;
	    default:
		intPart = intPart + ((float) (*c - '0')) * pFactor;
		pFactor = pFactor / 10.0;
		break;
	    }
	} else if (*c == '.') {
	    numOfPoint++;
	}
    }
    return sign * intPart;
}

/**
 * Convert IPv4 string to uint8_t[4].
 *
 * @param s the string in format "x.x.x.x"
 * @param resultIP the result. must be uint8_t[4]
 */
void strIPv4to4uint8(const char *s, uint8_t * resultIP)
{
    memset(resultIP, 0, 4);
    uint8_t r1;
    int i1;
    r1 = 0;
    i1 = 0;
    for (const char *c = s; *c != '\0'; c++) {
	if ('0' <= *c && *c <= '9') {
	    r1 *= 10;
	    r1 += ((uint8_t) (*c - '0'));
	} else if (*c == '.') {
	    resultIP[i1] = r1;
	    r1 = 0;
	    i1++;
	    if (i1 >= 4) {
		memset(resultIP, 0, 4);
		return;
	    }
	} else {
	    memset(resultIP, 0, 4);
	    return;
	}
    }
    if (i1 != 3) {
	memset(resultIP, 0, 4);
    } else {
	resultIP[i1] = r1;
    }
    return;
}

/**
 * Convert MAC addr string to uint8_t[6].
 *
 * @param s the string in format "XX:XX:XX:XX:XX:XX"
 * @param resultMAC the result. must be uint8_t[6]
 */
void strMACto6uint8(const char *s, uint8_t * resultMAC)
{
    memset(resultMAC, 0, 6);
    uint8_t r1;
    int i1;
    r1 = 0;
    i1 = 0;
    for (const char *c = s; *c != '\0'; c++) {
	if ('0' <= *c && *c <= '9') {
	    r1 *= 16;
	    r1 += ((uint8_t) (*c - '0'));
	} else if ('A' <= *c && *c <= 'F') {
	    r1 *= 16;
	    r1 += (((uint8_t) (*c - 'A')) + 10);
	} else if ('a' <= *c && *c <= 'f') {
	    r1 *= 16;
	    r1 += (((uint8_t) (*c - 'a')) + 10);
	} else if (*c == ':') {
	    resultMAC[i1] = r1;
	    r1 = 0;
	    i1++;
	    if (i1 >= 6) {
		memset(resultMAC, 0, 6);
		return;
	    }
	} else {
	    memset(resultMAC, 0, 6);
	    return;
	}
    }
    if (i1 != 5) {
	memset(resultMAC, 0, 6);
    } else {
	resultMAC[i1] = r1;
    }
    return;
}

/**
 * convert index to base64 char
 *
 * This is a variant version. AKA URL variant.
 * 62 should be + and 63 should be /. But we use - and _
 *
 * @param index the index value
 * @return the base64 character
 */
char base64urlChar(uint8_t index) {
  if (0 <= index && index <= 25) {
    return 'A'+index;
  } else if (26 <= index && index <= 51) {
    return 'a'+(index-26);
  } else if (52 <= index && index <= 61) {
    return '0'+(index-52);
  } else if (index == 62) {
    return '-';
  } else if (index == 63) {
    return '_';
  }
  return '\0';
}

/**
 * Convert MAC address to 8 byte string (base-64 url variant)
 *
 * @param mac MAC address
 * @param buf result buffer. At least 9 bytes size.
 */
void MACtoMM64(const uint8_t *mac, char *buf) {
  char *c = buf;
  const int n=6;
  for (int i=0; i<n; i+=3) {
    uint8_t value;
    value = mac[i] & 0x3f;
    *c = base64urlChar(value);
    c++;
    value = ((mac[i] >> 6) & 0x03);
    if (i+1 < n) {
      value |= ((mac[i+1] & 0x0f) << 2);
    }
    *c = base64urlChar(value);
    c++;
    if (!(i+1 < n)) {
      break;
    }
    value = ((mac[i+1] >> 4) & 0x0f);
    if (i+2 < n) {
      value |= ((mac[i+2] & 0x03) << 4);
    }
    *c = base64urlChar(value);
    c++;
    if (!(i+2 < n)) {
      break;
    }
    value = (mac[i+2] >> 2) & 0x3f;
    *c = base64urlChar(value);
    c++;
  }
  *c = '\0';
}

/**
 * Get a float from EEPROM
 *
 * @param addr address of EEPROM
 * @return the float
 */
float eepromGetFloat(int addr)
{
    uint8_t buf1[4];
    float ret;
    for (int i = 0; i < 4; i++) {
	buf1[i] = EEPROM.read(addr + i);
    }
    memcpy(&ret, buf1, 4);
    return ret;
}

/**
 * Put a float to EEPROM
 *
 * @param addr address of EEPROM
 * @param value the float value
 */
void eepromPutFloat(int addr, float value)
{
    uint8_t buf1[4];
    float data = eepromGetFloat(addr);
    if (data == value) {
	return;
    }
    memcpy(buf1, &value, 4);
    for (int i = 0; i < 4; i++) {
	EEPROM.write(addr + i, buf1[i]);
    }
}

/**
 * Get GPS position
 *
 * Get GPS position from EEPROM or GPS module.
 * It checks the fakeGPS flag in EEPROM. If fakeGPS is enabled,
 * the GPS data will come from EEPROM. Otherwise it queries the GPS
 * module for location
 *
 * @param lat latitude
 * @param lon longitude
 * @param alt altitude. Can be NULL if don't want to get altitude.
 * @param num the number of satellites. Can be NULL if don't care it.
 */
void getGPSPosition(float *lat, float *lon, float *alt, int *num)
{
    *lat = TinyGPS::GPS_INVALID_F_ANGLE;
    *lon = TinyGPS::GPS_INVALID_F_ANGLE;
    uint8_t fakeGPS;
    if (alt != NULL) {
	*alt = TinyGPS::GPS_INVALID_F_ALTITUDE;
    }
    if (num != NULL) {
	*num = -1;
    }
    fakeGPS = EEPROM.read(eepromAddrFakeGPS);
    if (fakeGPS) {
	float fakeGPSLat;
	float fakeGPSLon;
	float fakeGPSAlt;
	fakeGPSLat = eepromGetFloat(eepromAddrFakeGPSLat);
	fakeGPSLon = eepromGetFloat(eepromAddrFakeGPSLon);
	fakeGPSAlt = eepromGetFloat(eepromAddrFakeGPSAlt);
	if (!isnan(fakeGPSLat) && !isnan(fakeGPSLon) && !isnan(fakeGPSAlt)) {
	    *lat = fakeGPSLat;
	    *lon = fakeGPSLon;
	    if (num != NULL) {
		*num = 9;
	    }
	    if (alt != NULL) {
		*alt = fakeGPSAlt;
	    }
	}
    } else {
	gps.f_get_position(lat, lon);
	if (alt != NULL) {
	    *alt = gps.f_altitude();
	}
	if (num != NULL) {
	    *num = gps.satellites();
	}
    }
}

const unsigned long taskHandlePMSInterval = 10000;
unsigned long taskHandlePMS(void *p)
{
    serialPortPMS.listen();
    pms.readUntil(pmsData, 3000);
    return taskHandlePMSInterval;
}

const unsigned long taskHandleDHTInterval = 10000;
unsigned long taskHandleDHT(void *p)
{
    temperatureHumidValue[1] = dht.readTemperature();
    temperatureHumidValue[0] = dht.readHumidity();
    return taskHandleDHTInterval;
}

const unsigned long taskHandleGPSInterval = 10000;
unsigned long taskHandleGPS(void *p)
{
    uint8_t fakeGPS;
    fakeGPS = EEPROM.read(eepromAddrFakeGPS);
    if (fakeGPS) {
	return 60000;
    }
    serialPortGPS.listen();
    for (unsigned long start = millis(); diffMS(start, millis()) < 3000;) {
	if (serialPortGPS.available()) {
	    char c = serialPortGPS.read();
	    if (gps.encode(c)) {
		break;
	    }
	}
    }
    return taskHandleGPSInterval;
}

//#define DEBUG_ESP_BUF
/**
 * Check if we see a string in timeout time.
 *
 * @param str string to be checked. Should be a PSTR
 * @param timeout timeout in MS
 * @return 0 - not found, 1 - found
 */
int8_t espWaitForString_P(const char *str, int timeout)
{
    char buf[10];
    int buf_len;
    int strN = strlen_P(str);
    int8_t ret = 0;

    buf_len = 0;
    buf[0] = '\0';
    for (unsigned long start = millis();
	 diffMS(start, millis()) < timeout;) {
	if (serialPortESP.available()) {
	    char c = serialPortESP.read();
	    if (c == '\r' || c == '\n') {
		buf[buf_len] = '\0';
#ifdef DEBUG_ESP_BUF
		Serial.print(__func__);
		Serial.print(' ');
		Serial.println(buf);
#endif
		if (buf_len == strN && strncmp_P(buf, str, strN) == 0) {
		    ret = 1;
		    break;
		}
		buf[0] = '\0';
		buf_len = 0;
		continue;
	    }
	    if (buf_len + 1 < sizeof(buf)) {
		buf[buf_len] = c;
		buf_len++;
		buf[buf_len] = '\0';
	    }
	}
    }
    return ret;
}

/**
 * Check if we see a char in timeout time.
 *
 * @param c char to be checked.
 * @param timeout timeout in MS
 * @return 0 - not found, 1 - found
 */
int8_t espWaitForChar(char c, int timeout) {
    int8_t ret=0;
    for (unsigned long start = millis(); diffMS(start, millis()) < timeout;) {
	if (serialPortESP.available()) {
	    char c1 = serialPortESP.read();
	    if (c1 == c) {
		ret = 1;
		break;
	    }
	}
    }
    return ret;
}


/**
 * Parse return parameters with delim
 *
 * @param prefix string to be match. Should be a PSTR
 * @param delim a delimiter
 * @param timeout timeout in MS
 * @param buf working area. Should be long enough.
 *        (size at least prefix+2 delim+length of parameter).
 * @param bufSize The total size of the buf.
 *        We also put the results here.
 * @return 0 - not found, 1 - found
 */
int8_t
espParseResultWithDelim_P(const char *prefix, char delim, int timeout,
			  char *buf, int bufSize)
{
    int buf_len = 0;
    int prefixLen = strlen_P(prefix);
    int8_t ret = 0;

    buf[0] = '\0';
    for (unsigned long start = millis();
	 diffMS(start, millis()) < timeout;) {
	if (serialPortESP.available()) {
	    char c = serialPortESP.read();
	    if (c == '\r' || c == '\n') {
		if (buf_len > prefixLen
		    && strncmp_P(buf, prefix, prefixLen) == 0) {
		    char *left;
		    char *right;
		    if (delim != '\0') {
			left = strchr(&(buf[prefixLen]), delim);
			right = strrchr(&(buf[prefixLen]), delim);
		    } else {
			left = &(buf[prefixLen - 1]);
			right = &(buf[buf_len]);
		    }
		    if (left != NULL && right != NULL && left < right) {
			char *b1 = buf;
			char *c1 = left;
			c1++;
			for (; c1 < right && (*c1 != '\0'); c1++) {
			    *b1 = *c1;
			    b1++;
			}
			*b1 = '\0';
			ret = 1;
			break;
		    }
		}
		buf[0] = '\0';
		buf_len = 0;
		continue;
	    }
	    if (buf_len + 1 < bufSize) {
		buf[buf_len] = c;
		buf_len++;
		buf[buf_len] = '\0';
	    }
	}
    }
    return ret;
}

const unsigned long taskHandleESP_Status_Interval = 60000;
unsigned long taskHandleESP_Status(void *p)
{
    char buf[40];
    int buf_len = 0;
    int8_t gotOK;

    serialPortESP.listen();

    /* Get CIP status so we can know if ESP-01S is connected or not */
    espCIPStatus = -1;
    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPSTATUS"));
    gotOK =
	espParseResultWithDelim_P(PSTR("STATUS:"), '\0', 2000, buf,
				  sizeof(buf));
    if (gotOK) {
	espCIPStatus = buf[0] - '0';
    } else {
	espCIPStatus = -1;
    }

    /* Get the AP IP address */
    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPAP?"));
    gotOK =
	espParseResultWithDelim_P(PSTR("+CIPAP:ip:"), '\"', 2000, buf,
				  sizeof(buf));
    if (gotOK) {
	strIPv4to4uint8(buf, IP_AP);
    } else {
	memset(IP_AP, 0, sizeof(IP_AP));
    }

    /* Get the client address */
    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPSTA?"));
    gotOK =
	espParseResultWithDelim_P(PSTR("+CIPSTA:ip:"), '\"', 2000, buf,
				  sizeof(buf));
    if (gotOK) {
	strIPv4to4uint8(buf, IP_STA);
    } else {
	memset(IP_STA, 0, sizeof(IP_STA));
    }

    /* Get MAC address */
    if (MAC_STA[0] == 0 && MAC_STA[1] == 0 && MAC_STA[2] == 0 && MAC_STA[3] == 0 && MAC_STA[4] == 0 && MAC_STA[5] == 0) {
      /* MAC address don't change. We only need to query once */
      serialPortESP.flush();
      serialPortESP.println(F("AT+CIPSTAMAC?"));
      gotOK =
	  espParseResultWithDelim_P(PSTR("+CIPSTAMAC:"), '\"', 2000, buf,
				    sizeof(buf));
      if (gotOK) {
	  strMACto6uint8(buf, MAC_STA);
      } else {
	  memset(MAC_STA, 0, sizeof(MAC_STA));
      }
    }

    if (espCIPStatus != 2 && espCIPStatus != 3 && espCIPStatus != 4) {
	return 10000;
    }

    return taskHandleESP_Status_Interval;
}

const unsigned long taskHandleESP_NTP_Interval = 1000000;
const unsigned long taskHandleESP_NTP_Fail_Interval = 10000;
unsigned long taskHandleESP_NTP(void *p)
{
    const int sntpPacketSize = 48;
    byte sntpPacket[4];		// We only care 4 bytes in packet.
    int8_t gotOK;
    int pIPD48Counter = -8;

    if (espCIPStatus != 2 && espCIPStatus != 3 && espCIPStatus != 4) {
	return taskHandleESP_NTP_Fail_Interval;
    }

    serialPortESP.listen();

    /* Get time from NTP server. We will try to get the time each
       1000000 ms ~= 16 minutes. We will calculate the time by NTP server
       + arduino millis in this 16 minutes. This avoid frequently contacting
       the NTP server and slow things down. */

    serialPortESP.flush();
    serialPortESP.println(F
			  ("AT+CIPSTART=\"UDP\",\"time.stdtime.gov.tw\",123"));
    gotOK = espWaitForString_P(PSTR("OK"), 3000);
    serialPortESP.flush();
    if (gotOK == 0) {
#ifdef DEBUG_ESP_SNTP
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got OK from START"));
#endif				/* DEBUG_ESP_SNTP */
	goto taskHandleESP_NTP_STOP;
    }

    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPSEND=48"));
    gotOK = espWaitForString_P(PSTR("OK"), 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_SNTP
	Serial.println(F("Error: didn't got OK from SEND"));
#endif				/* DEBUG_ESP_SNTP */
	goto taskHandleESP_NTP_STOP;
    }
    gotOK = espWaitForChar('>', 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_SNTP
	Serial.print(__func__);
	Serial.println(F("Error: didn't got > from SEND"));
#endif				/* DEBUG_ESP_SNTP */
	goto taskHandleESP_NTP_STOP;
    }

    memset(sntpPacket, 0, sizeof(sntpPacket));
    sntpPacket[0] = 0b11100011;
    sntpPacket[1] = 0;
    sntpPacket[2] = 6;
    sntpPacket[3] = 0xec;
    for (int i = 0; i < sntpPacketSize; i++) {
	if (i < sizeof(sntpPacket)) {
	    serialPortESP.write(sntpPacket[i]);
	} else {
	    /* rest bytes are 0 */
	    serialPortESP.write((uint8_t) 0);
	}
    }

    pIPD48Counter = -8;
    for (unsigned long start = millis(); diffMS(start, millis()) < 10000;) {
	if (serialPortESP.available()) {
	    char c = serialPortESP.read();
	    if (pIPD48Counter == -8 && c == '+') {
		pIPD48Counter++;
	    } else if (pIPD48Counter == -7 && c == 'I') {
		pIPD48Counter++;
	    } else if (pIPD48Counter == -6 && c == 'P') {
		pIPD48Counter++;
	    } else if (pIPD48Counter == -5 && c == 'D') {
		pIPD48Counter++;
	    } else if (pIPD48Counter == -4 && c == ',') {
		pIPD48Counter++;
	    } else if (pIPD48Counter == -3 && c == '4') {
		pIPD48Counter++;
	    } else if (pIPD48Counter == -2 && c == '8') {
		pIPD48Counter++;
	    } else if (pIPD48Counter == -1 && c == ':') {
		pIPD48Counter++;
		memset(sntpPacket, 0, sizeof(sntpPacket));
	    } else if (pIPD48Counter >= 0
		       && pIPD48Counter < sntpPacketSize) {
		if (40 <= pIPD48Counter && pIPD48Counter <= 43) {
		    sntpPacket[pIPD48Counter - 40] = c;
		}
		pIPD48Counter++;
		if (pIPD48Counter >= sntpPacketSize) {
		    break;
		}
	    }
	}
    }
    if (pIPD48Counter >= sntpPacketSize) {
	unsigned long highWord = word(sntpPacket[0], sntpPacket[1]);
	unsigned long lowWord = word(sntpPacket[2], sntpPacket[3]);
	unsigned long secsSince1900 = highWord << 16 | lowWord;
	const unsigned long seventyYears = 2208988800UL;
	unsigned long epoch = secsSince1900 - seventyYears;
	if (secsSince1900 > seventyYears) {
	    sntpEpoch = epoch;
	    sntpPrevMS = millis();
	}
    }

  taskHandleESP_NTP_STOP:

    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPCLOSE"));
    gotOK = espWaitForString_P(PSTR("OK"), 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_SNTP
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got OK from CLOSE"));
#endif				/* DEBUG_ESP_SNTP */
    }
    serialPortESP.flush();

    if (sntpPrevMS == 0
	|| diffMS(sntpPrevMS, millis()) > taskHandleESP_NTP_Interval) {
	return taskHandleESP_NTP_Fail_Interval;
    }
    return taskHandleESP_NTP_Interval;
}

//#define DEBUG_ESP_MQTT
const unsigned long taskHandleESP_MQTT_Interval = 10000;
unsigned long taskHandleESP_MQTT(void *p)
{
    int8_t gotOK;
    int n1;
    struct tm tm1;
    unsigned long newEpoch;
    uint16_t tmp2;
    float gpsLAT;
    float gpsLON;
    float gpsALT;
    int gpsNUM;
    float tmp3, tmp4;

    int mqtt_packet_len;
    const uint8_t header_control = 0x30;
    uint8_t header_remaining_length[4];
    int header_remaining_length_len;
    uint16_t remaining_length;

    uint16_t topic_length;
    static const char topic[] PROGMEM = {
	"LASS/Test/PM25"
    };

    uint16_t payload_length;
    static const char payload_ver_format[] PROGMEM = {
	"|ver_format=3"
    };
    static const char payload_fakegps_field[] PROGMEM = {
	"|FAKE_GPS="
    };
    char payload_fakegps_data;
    static const char payload_app[] PROGMEM = {
	"|app=PM25|ver_app=0.7.3"
    };
    static const char payload_deviceid_field[] PROGMEM = {
	"|device=LASSWONKT|device_id="
    };
    char payload_deviceid_data[14];
    static const char payload_tick_field[] PROGMEM = {
	"|tick="
    };
    char payload_tick_data[12];
    static const char payload_date_field[] PROGMEM = {
	"|date="
    };
    char payload_date_data[11];
    static const char payload_time_field[] PROGMEM = {
	"|time="
    };
    char payload_time_data[9];
    static const char payload_sd0_field[] PROGMEM = {
	"|s_d0="
    };
    char payload_sd0_data[6];
    static const char payload_sd1_field[] PROGMEM = {
	"|s_d1="
    };
    char payload_sd1_data[6];
    static const char payload_st0_field[] PROGMEM = {
	"|s_t0="
    };
    char payload_st0_data[6];
    static const char payload_sh0_field[] PROGMEM = {
	"|s_h0="
    };
    char payload_sh0_data[6];
    static const char payload_gpsLON_field[] PROGMEM = {
	"|gps_lon="
    };
    char payload_gpsLON_data[11];
    static const char payload_gpsLAT_field[] PROGMEM = {
	"|gps_lat="
    };
    char payload_gpsLAT_data[11];
    static const char payload_gpsALT_field[] PROGMEM = {
	"|gps_alt="
    };
    char payload_gpsALT_data[6];
    static const char payload_gpsNUM_field[] PROGMEM = {
	"|gps_fix=0|gps_num="
    };
    char payload_gpsNUM_data[5];

    /* Check WiFi status. If there's no Wifi, quit. */
    if (espCIPStatus != 2 && espCIPStatus != 3 && espCIPStatus != 4) {
	return taskHandleESP_MQTT_Interval;
    }

    /* Sanity check - We don't send insane data to the server */
    gotOK = 1;
    if (sntpEpoch == 0
	|| (pmsData.PM_AE_UG_2_5 == 0 && pmsData.PM_AE_UG_10_0 == 0)
	|| (temperatureHumidValue[0] == 0.0
	    && temperatureHumidValue[1] == 0.0)
	|| isnan(temperatureHumidValue[0])
	|| isnan(temperatureHumidValue[1])
        || (MAC_STA[0] == 0 && MAC_STA[1] == 0 && MAC_STA[2] == 0 && MAC_STA[3] == 0 && MAC_STA[4] == 0 && MAC_STA[5] == 0)
    ) {
	gotOK = 0;
    }
    getGPSPosition(&gpsLAT, &gpsLON, &gpsALT, &gpsNUM);
    if ( isnan(gpsLAT) 
      || isnan(gpsLON)
      || isnan(gpsALT)
      || gpsLAT == TinyGPS::GPS_INVALID_F_ANGLE
      || gpsLON == TinyGPS::GPS_INVALID_F_ANGLE
      || gpsALT == TinyGPS::GPS_INVALID_F_ALTITUDE
      || gpsNUM < 0
      || (! (-90.00001<= gpsLAT && gpsLAT <= 90.00001))
      || (! (-180.00001 <= gpsLON && gpsLON <= 180.00001))
      ) {
        gotOK = 0;
    }
    if (gotOK == 0) {
	return taskHandleESP_MQTT_Interval;
    }

    /* prepare MQTT data */

    topic_length = strlen_P(topic);

    if (EEPROM.read(eepromAddrFakeGPS) != 0) {
	payload_fakegps_data = '1';
    } else {
	payload_fakegps_data = '0';
    }

    strncpy_P(payload_deviceid_data, PSTR("WNKT-"),
	    sizeof(payload_deviceid_data));
    MACtoMM64(MAC_STA, &(payload_deviceid_data[5]));
    payload_deviceid_data[sizeof(payload_deviceid_data) - 1] = '\0';

    snprintf_P(payload_tick_data, sizeof(payload_tick_data), PSTR("%lu"),
	     millis());
    payload_tick_data[sizeof(payload_tick_data) - 1] = '\0';

    newEpoch =
	sntpEpoch + diffMS(sntpPrevMS, millis()) / 1000 - UNIX_OFFSET;
    gmtime_r(&newEpoch, &tm1);
    snprintf_P(payload_date_data, sizeof(payload_date_data),
	     PSTR("%04d-%02d-%02d"), tm1.tm_year + 1900, tm1.tm_mon + 1,
	     tm1.tm_mday);
    payload_date_data[sizeof(payload_date_data) - 1] = '\0';
    snprintf_P(payload_time_data, sizeof(payload_time_data),
	     PSTR("%02d:%02d:%02d"), tm1.tm_hour, tm1.tm_min, tm1.tm_sec);
    payload_time_data[sizeof(payload_time_data) - 1] = '\0';

    snprintf_P(payload_sd0_data, sizeof(payload_sd0_data),
             PSTR("%d"), pmsData.PM_AE_UG_2_5);
    payload_sd0_data[sizeof(payload_sd0_data) - 1] = '\0';
    snprintf_P(payload_sd1_data, sizeof(payload_sd1_data),
             PSTR("%d"), pmsData.PM_AE_UG_10_0);
    payload_sd1_data[sizeof(payload_sd1_data) - 1] = '\0';

    snprintf_P(payload_st0_data, sizeof(payload_st0_data),
             PSTR("%d"), (int)roundf(temperatureHumidValue[1]));
    payload_st0_data[sizeof(payload_st0_data) - 1] = '\0';
    snprintf_P(payload_sh0_data, sizeof(payload_sh0_data),
             PSTR("%d"), (int)roundf(temperatureHumidValue[0]));
    payload_sh0_data[sizeof(payload_sh0_data) - 1] = '\0';

    tmp3 = modff(gpsLAT, &tmp4);
    snprintf_P(payload_gpsLAT_data, sizeof(payload_gpsLAT_data),
             PSTR("%d.XXXXX"), (int)(tmp4));
    payload_gpsLAT_data[sizeof(payload_gpsLAT_data) - 1] = '\0';
    tmp3 = modff(gpsLON, &tmp4);
    snprintf_P(payload_gpsLON_data, sizeof(payload_gpsLON_data),
             PSTR("%d.XXXXX"), (int)(tmp4));
    payload_gpsLON_data[sizeof(payload_gpsLON_data) - 1] = '\0';
    snprintf_P(payload_gpsALT_data, sizeof(payload_gpsALT_data),
             PSTR("%d"), (int)roundf(gpsALT));
    payload_gpsALT_data[sizeof(payload_gpsALT_data) - 1] = '\0';
    snprintf_P(payload_gpsNUM_data, sizeof(payload_gpsNUM_data),
             PSTR("%d"), gpsNUM);
    payload_gpsNUM_data[sizeof(payload_gpsNUM_data) - 1] = '\0';


    payload_length = strlen_P(payload_ver_format)
	+ strlen_P(payload_fakegps_field)
	+ sizeof(payload_fakegps_data)
	+ strlen_P(payload_app)
	+ strlen_P(payload_deviceid_field)
	+ strlen(payload_deviceid_data)
	+ strlen_P(payload_tick_field)
	+ strlen(payload_tick_data)
	+ strlen_P(payload_date_field)
	+ strlen(payload_date_data)
	+ strlen_P(payload_time_field)
	+ strlen(payload_time_data)
	+ strlen_P(payload_sd0_field)
	+ strlen(payload_sd0_data)
	+ strlen_P(payload_sd1_field)
	+ strlen(payload_sd1_data)
	+ strlen_P(payload_st0_field)
	+ strlen(payload_st0_data)
	+ strlen_P(payload_sh0_field)
	+ strlen(payload_sh0_data)
	+ strlen_P(payload_gpsLAT_field)
	+ strlen(payload_gpsLAT_data)
	+ strlen_P(payload_gpsLON_field)
	+ strlen(payload_gpsLON_data)
	+ strlen_P(payload_gpsALT_field)
	+ strlen(payload_gpsALT_data)
	+ strlen_P(payload_gpsNUM_field)
	+ strlen(payload_gpsNUM_data);

    remaining_length = sizeof(topic_length)
	+ topic_length + payload_length;
    tmp2 = remaining_length;
    for (int i = 0;
	 i <
	 sizeof(header_remaining_length) /
	 sizeof(header_remaining_length[0]); i++) {
	header_remaining_length[i] = tmp2 % 128;
	tmp2 /= 128;
	if (tmp2 > 0) {
	    header_remaining_length[i] |= 0x80;
	} else {
	    header_remaining_length_len = i + 1;
	    break;
	}
    }

    mqtt_packet_len = sizeof(header_control)
	+ header_remaining_length_len + remaining_length;

    serialPortESP.listen();

    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPSTART=\"TCP\",\"gpssensor.ddns.net\",1883"));
    gotOK = espWaitForString_P(PSTR("OK"), 3000);
    serialPortESP.flush();
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got OK from START"));
#endif				/* DEBUG_ESP_MQTT */
	goto taskHandleESP_MQTT_STOP;
    }

    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPSEND=27"));
    gotOK = espWaitForString_P(PSTR("OK"), 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got OK from SEND"));
#endif				/* DEBUG_ESP_MQTT */
	goto taskHandleESP_MQTT_STOP;
    }
    gotOK = espWaitForChar('>', 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got > from SEND"));
#endif				/* DEBUG_ESP_MQTT */
	goto taskHandleESP_MQTT_STOP;
    }

    tmp2 = strlen(payload_deviceid_data);
    serialPortESP.write('\x10');
    serialPortESP.write(10+2+tmp2);
    serialPortESP.write((uint8_t)0);
    serialPortESP.write('\x04');
    serialPortESP.print(F("MQTT"));
    serialPortESP.write('\x04');
    serialPortESP.write('\x02');
    serialPortESP.write((uint8_t)0);
    serialPortESP.write('\x3c');
    serialPortESP.write(tmp2/256);
    serialPortESP.write(tmp2%256);
    serialPortESP.print(payload_deviceid_data);

    gotOK = espWaitForString_P(PSTR("SEND OK"), 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got SEND OK after SEND"));
#endif				/* DEBUG_ESP_MQTT */
    }
    
    serialPortESP.flush();
    serialPortESP.print(F("AT+CIPSEND="));
    serialPortESP.println(mqtt_packet_len);
    gotOK = espWaitForString_P(PSTR("OK"), 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got OK from SEND"));
#endif				/* DEBUG_ESP_MQTT */
	goto taskHandleESP_MQTT_STOP;
    }
    gotOK = espWaitForChar('>', 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got > from SEND"));
#endif				/* DEBUG_ESP_MQTT */
	goto taskHandleESP_MQTT_STOP;
    }

    /* send out MQTT packet */
    serialPortESP.write(header_control);
    for (int i = 0; i < header_remaining_length_len; i++) {
	serialPortESP.write(header_remaining_length[i]);
    }

    serialPortESP.write(topic_length / 256);
    serialPortESP.write(topic_length % 256);
    serialPortESP.print(FPSTR001(topic));

    serialPortESP.print(FPSTR001(payload_ver_format));
    serialPortESP.print(FPSTR001(payload_fakegps_field));
    serialPortESP.print(payload_fakegps_data);
    serialPortESP.print(FPSTR001(payload_app));
    serialPortESP.print(FPSTR001(payload_deviceid_field));
    serialPortESP.print(payload_deviceid_data);
    serialPortESP.print(FPSTR001(payload_tick_field));
    serialPortESP.print(payload_tick_data);
    serialPortESP.print(FPSTR001(payload_date_field));
    serialPortESP.print(payload_date_data);
    serialPortESP.print(FPSTR001(payload_time_field));
    serialPortESP.print(payload_time_data);
    serialPortESP.print(FPSTR001(payload_sd0_field));
    serialPortESP.print(payload_sd0_data);
    serialPortESP.print(FPSTR001(payload_sd1_field));
    serialPortESP.print(payload_sd1_data);
    serialPortESP.print(FPSTR001(payload_st0_field));
    serialPortESP.print(payload_st0_data);
    serialPortESP.print(FPSTR001(payload_sh0_field));
    serialPortESP.print(payload_sh0_data);
    serialPortESP.print(FPSTR001(payload_gpsLAT_field));
    serialPortESP.print(gpsLAT,5);
    serialPortESP.print(FPSTR001(payload_gpsLON_field));
    serialPortESP.print(gpsLON,5);
    serialPortESP.print(FPSTR001(payload_gpsALT_field));
    serialPortESP.print(payload_gpsALT_data);
    serialPortESP.print(FPSTR001(payload_gpsNUM_field));
    serialPortESP.print(payload_gpsNUM_data);

    gotOK = espWaitForString_P(PSTR("SEND OK"), 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got SEND OK after SEND"));
#endif				/* DEBUG_ESP_MQTT */
    }

  taskHandleESP_MQTT_STOP:
    serialPortESP.flush();
    serialPortESP.println(F("AT+CIPCLOSE"));
    gotOK = espWaitForString_P(PSTR("OK"), 1000);
    if (gotOK == 0) {
#ifdef DEBUG_ESP_MQTT
	Serial.print(__func__);
	Serial.println(F(" Error: didn't got OK from CLOSE"));
#endif				/* DEBUG_ESP_MQTT */
    }
    serialPortESP.flush();

    return taskHandleESP_MQTT_Interval;
}

const unsigned long taskHandleLCDInterval = 1000;
unsigned long taskHandleLCD(void *p)
{
    static float prevTemperatureValue = -1;
    static float prevHumidValue = -1;
    static float prevPM25Value = -1;
    static float prevLatitude = -1;
    static float prevLongitude = -1;
    static unsigned long pageTime = 0;
    static int8_t page = 0;
    static int8_t oldEpochSec = 0;

    int8_t dirtyFlag = 0;
    float currentLatitude = -1;
    float currentLongitude = -1;
    unsigned long newEpoch;

    if (diffMS(pageTime, millis()) > 5000) {
	pageTime = millis();
	page = (page + 1) % 4;
	dirtyFlag = 1;
    }
    switch (page) {
    case 0:
	if (temperatureHumidValue[1] != prevTemperatureValue) {
	    dirtyFlag = 1;
	}
	if (temperatureHumidValue[0] != prevHumidValue) {
	    dirtyFlag = 1;
	}
	if (pmsData.PM_AE_UG_2_5 != prevPM25Value) {
	    dirtyFlag = 1;
	}
	break;
    case 1:
	getGPSPosition(&currentLatitude, &currentLongitude, NULL, NULL);
	if (prevLatitude != currentLatitude) {
	    dirtyFlag = 1;
	}
	if (prevLongitude != currentLongitude) {
	    dirtyFlag = 1;
	}
	break;
    case 2:
	break;
    case 3:
	newEpoch =
	    sntpEpoch + diffMS(sntpPrevMS, millis()) / 1000 - UNIX_OFFSET;
	if (oldEpochSec != (newEpoch % 60)) {
	    dirtyFlag = 1;
	}
	break;
    default:
	break;
    }

    if (!dirtyFlag) {
	return taskHandleLCDInterval;
    }
    lcd.clear();
    switch (page) {
    case 0:
	lcd.setCursor(0, 0);
	lcd.print(F("T:"));
	lcd.print(temperatureHumidValue[1]);
	lcd.setCursor(8, 0);
	lcd.print(F("H:"));
	lcd.print(temperatureHumidValue[0]);
	lcd.setCursor(0, 1);
	lcd.print(F("PM2.5:"));
	lcd.print(pmsData.PM_AE_UG_2_5);
	prevTemperatureValue = temperatureHumidValue[1];
	prevHumidValue = temperatureHumidValue[0];
	prevPM25Value = pmsData.PM_AE_UG_2_5;
	break;
    case 1:
	lcd.setCursor(0, 0);
	lcd.print(F("Lat: "));
	if (currentLatitude == TinyGPS::GPS_INVALID_F_ANGLE) {
	    lcd.print(F("---"));
	} else {
	    lcd.print(currentLatitude, 5);
	}
	lcd.setCursor(0, 1);
	lcd.print(F("Lon: "));
	if (currentLongitude == TinyGPS::GPS_INVALID_F_ANGLE) {
	    lcd.print(F("---"));
	} else {
	    lcd.print(currentLongitude, 5);
	}
	prevLatitude = currentLatitude;
	prevLongitude = currentLongitude;
	break;
    case 2:
	lcd.setCursor(0, 0);
	if (!
	    (IP_AP[0] == 0 && IP_AP[1] == 0 && IP_AP[2] == 0
	     && IP_AP[3] == 0)) {
	    for (int i = 0; i < 4; i++) {
		if (i > 0) {
		    lcd.print('.');
		}
		lcd.print(IP_AP[i]);
	    }
	} else {
	    lcd.print(F("AP NOT READY"));
	}
	lcd.setCursor(0, 1);
	if (!
	    (IP_STA[0] == 0 && IP_STA[1] == 0 && IP_STA[2] == 0
	     && IP_STA[3] == 0)) {
	    for (int i = 0; i < 4; i++) {
		if (i > 0) {
		    lcd.print('.');
		}
		lcd.print(IP_STA[i]);
	    }
	} else {
	    lcd.print(F("WIFI NOT CONNECT"));
	}
	break;
    case 3:
	if (sntpEpoch != 0) {
	    struct tm tm1;
            char buf2[20];
	    newEpoch =
		sntpEpoch + diffMS(sntpPrevMS,
				   millis()) / 1000 - UNIX_OFFSET;
	    int8_t timezoneH = (int8_t) EEPROM.read(eepromAddrTimezoneH);
	    newEpoch += ((int) (timezoneH)) * 60 * 60;
	    gmtime_r(&newEpoch, &tm1);
	    lcd.setCursor(0, 0);
            snprintf_P(buf2,sizeof(buf2),PSTR("%04d-%02d-%02d"), tm1.tm_year + 1900, tm1.tm_mon+1, tm1.tm_mday);
            lcd.print(buf2);
	    lcd.setCursor(0, 1);
            snprintf_P(buf2,sizeof(buf2),PSTR("%02d:%02d:%02d"), tm1.tm_hour, tm1.tm_min, tm1.tm_sec);
            lcd.print(buf2);
            oldEpochSec = (newEpoch % 60);
	} else {
	    lcd.setCursor(2, 0);
	    lcd.print(F("uCRobotics"));
	    lcd.setCursor(4, 1);
	    lcd.print(F("Taiwan"));
	}
	break;
    default:
	break;
    }
    return taskHandleLCDInterval;
}

/**
 * port forwarding
 *
 * This function forward a SoftwareSerial to Serial.
 * It stops when see a breakChar appeared from Serial.
 *
 * @param s the SoftwareSerial object
 * @param breakChar the char to break the port forwarding.
 */
void portForwarding(SoftwareSerial &s, char breakChar) {
  s.listen();
  int incomingByte;
  while (true) {
    if (s.available()) {
        incomingByte = s.read();
        Serial.write(incomingByte);
    }
    if (Serial.available()) {
        incomingByte = Serial.read();
        if (breakChar && incomingByte == breakChar) {
	  break;
        }
        s.write(incomingByte);
    }
  }
}

/**
 * Execute a command from Serial console
 *
 * @param cmd the command comes from the Serial port
 */
void runCommand(char *cmd)
{
    char *saveptr1;
    char *cmd1;
    char *subcmd1;
    char *var;
    char *arg;
    if (strncmp_P(cmd, PSTR("printenv"), 8) == 0) {
	uint8_t fakeGPS;
	float fakeGPSLat;
	float fakeGPSLon;
	float fakeGPSAlt;
	int8_t timezoneH;
	Serial.print(F("fakeGPS="));
	fakeGPS = EEPROM.read(eepromAddrFakeGPS);
	Serial.println((int) fakeGPS);
	Serial.print(F("fakeGPSLat="));
	fakeGPSLat = eepromGetFloat(eepromAddrFakeGPSLat);
	Serial.println(fakeGPSLat, 5);
	Serial.print(F("fakeGPSLon="));
	fakeGPSLon = eepromGetFloat(eepromAddrFakeGPSLon);
	Serial.println(fakeGPSLon, 5);
	Serial.print(F("fakeGPSAlt="));
	fakeGPSAlt = eepromGetFloat(eepromAddrFakeGPSAlt);
	Serial.println(fakeGPSAlt);
	Serial.print(F("timezoneH="));
	timezoneH = (int8_t) EEPROM.read(eepromAddrTimezoneH);
	Serial.println((int) timezoneH);
    } else if (strncmp_P(cmd, PSTR("help"), 4) == 0) {
	Serial.println(F
		       ("printenv - print env vars"));
	Serial.println(F("setenv - set env vars"));
	Serial.println(F("eeprom - eeprom cmds"));
	Serial.println(F("pms - PMS cmds"));
	Serial.println(F("gps - GPS cmds"));
	Serial.println(F("esp - ESP cmds"));
	Serial.println(F("dht - DHT cmds"));
	Serial.println(F("free - free mem size"));
      } else if (strncmp_P(cmd, PSTR("setenv"), 6) == 0) {
	saveptr1 = NULL;
	cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
	var = strtok_rP(NULL, PSTR(" "), &saveptr1);
	if (var != NULL) {
	    if (strcmp_P(var, PSTR("fakeGPS")) == 0) {
		arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
		if (arg != NULL) {
		    unsigned int data = 0;
		    if (sscanf_P(arg, PSTR("%u"), &data) == 1) {
			uint8_t fakeGPS;
			uint8_t newFakeGPS = (uint8_t) data;
			fakeGPS = EEPROM.read(eepromAddrFakeGPS);
			if (fakeGPS != newFakeGPS) {
			    EEPROM.write(eepromAddrFakeGPS, newFakeGPS);
			}
		    }
		}
	    } else if (strcmp_P(var, PSTR("fakeGPSLat")) == 0) {
		arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
		if (arg != NULL) {
		    float data = 0.0;
		    data = str2float(arg);
		    if (!isnan(data)) {
			eepromPutFloat(eepromAddrFakeGPSLat, data);
		    }
		}
	    } else if (strcmp_P(var, PSTR("fakeGPSLon")) == 0) {
		arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
		if (arg != NULL) {
		    float data = 0.0;
		    data = str2float(arg);
		    if (!isnan(data)) {
			eepromPutFloat(eepromAddrFakeGPSLon, data);
		    }
		}
	    } else if (strcmp_P(var, PSTR("timezoneH")) == 0) {
		arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
		if (arg != NULL) {
		    int data = 0;
		    if (sscanf_P(arg, PSTR("%d"), &data) == 1) {
			int8_t oldTimezoneH;
			int8_t newTimezoneH = (int8_t) data;
			oldTimezoneH = EEPROM.read(eepromAddrTimezoneH);
			if (oldTimezoneH != newTimezoneH) {
			    EEPROM.write(eepromAddrTimezoneH,
					 (uint8_t) newTimezoneH);
			}
		    }
		}
	    } else if (strcmp_P(var, PSTR("fakeGPSAlt")) == 0) {
		arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
		if (arg != NULL) {
		    float data = 0.0;
		    data = str2float(arg);
		    if (!isnan(data)) {
			eepromPutFloat(eepromAddrFakeGPSAlt, data);
		    }
		}
	    }
	}
    } else if (strncmp_P(cmd, PSTR("eeprom"), 6) == 0) {
	saveptr1 = NULL;
	cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
	subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
	if (subcmd1 == NULL) {
	    Serial.println(F("eeprom dump - dump EEPROM"));
	    Serial.println(F("eeprom setb <addr(DEC)> <value(HEX)>"));
	} else if (strcmp_P(subcmd1, PSTR("dump")) == 0) {
	    for (int i = 0; i < 512; i++) {
		int data;
		if (i % 16 > 0) {
		    Serial.print(' ');
		}
		if (i % 16 == 8) {
		    Serial.print(' ');
		}
		data = EEPROM.read(i);
		if (data <= 0x0f) {
		    Serial.print(0, HEX);
		}
		Serial.print(data, HEX);
		if (i % 16 == 15) {
		    Serial.println();
		}
	    }
	} else if (strcmp_P(subcmd1, PSTR("setb")) == 0) {
	    var = strtok_rP(NULL, PSTR(" "), &saveptr1);
	    if (var != NULL) {
		int eAddrI;
		if (sscanf_P(var, PSTR("%d"), &eAddrI) == 1) {
		    arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
		    if (arg != NULL) {
			int hexValueI;
			if (sscanf_P(arg, PSTR("%x"), &hexValueI) ==
			    1) {
			    EEPROM.write(eAddrI, hexValueI);
			}
		    }
		}
	    }
	}
    } else if (strncmp_P(cmd, PSTR("free"), 4) == 0) {
	Serial.print(F("Mem: "));
#ifdef CFG_ENABLE_MEMORYFREE
	Serial.println(freeMemory());
#else
	Serial.println(F("Not supported"));
#endif
    } else if (strncmp_P(cmd, PSTR("pms"), 3) == 0) {
	saveptr1 = NULL;
	cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
	subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
	if (subcmd1 == NULL) {
	    Serial.println(F("pms status - Show stat"));
	    Serial.println(F
			   ("pms shell - Direct commu with module"));
	} else if (strcmp_P(subcmd1, PSTR("status")) == 0) {
	    Serial.print(F("pms_pm1.0="));
	    Serial.println(pmsData.PM_AE_UG_1_0);
	    Serial.print(F("pms_pm2.5="));
	    Serial.println(pmsData.PM_AE_UG_2_5);
	    Serial.print(F("pms_pm10="));
	    Serial.println(pmsData.PM_AE_UG_10_0);
	} else if (strcmp_P(subcmd1, PSTR("shell")) == 0) {
            portForwarding(serialPortPMS, '\x03');
	}
    } else if (strncmp_P(cmd, PSTR("esp"), 3) == 0) {
	saveptr1 = NULL;
	cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
	subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
	if (subcmd1 == NULL) {
	    unsigned long newEpoch;
	    Serial.println(F("esp status - Show stat"));
	    Serial.println(F
			   ("esp shell - Direct commu with module"));
	} else if (strcmp_P(subcmd1, PSTR("status")) == 0) {
	    Serial.print(F("CIPSTATUS="));
	    Serial.println(espCIPStatus);
	    Serial.print(F("Epoch="));
	    Serial.println(sntpEpoch);
	    Serial.print(F("IP_AP="));
	    for (int i = 0; i < 4; i++) {
		if (i > 0) {
		    Serial.print('.');
		}
		Serial.print(IP_AP[i]);
	    }
	    Serial.println();
	    Serial.print(F("IP_STA="));
	    for (int i = 0; i < 4; i++) {
		if (i > 0) {
		    Serial.print('.');
		}
		Serial.print(IP_STA[i]);
	    }
	    Serial.println();
	} else if (strcmp_P(subcmd1, PSTR("shell")) == 0) {
            portForwarding(serialPortESP, '\x03');
	}
    } else if (strncmp_P(cmd, PSTR("dht"), 3) == 0) {
	saveptr1 = NULL;
	cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
	subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
	if (subcmd1 == NULL) {
	    Serial.println(F("dht status - Show stat"));
	} else if (strcmp_P(subcmd1, PSTR("status")) == 0) {
	    Serial.print(F("dht_temperature="));
	    Serial.println(temperatureHumidValue[1]);
	    Serial.print(F("dht_humid="));
	    Serial.println(temperatureHumidValue[0]);
	}
    } else if (strncmp_P(cmd, PSTR("gps"), 3) == 0) {
	float latitude;
	float longitude;
	saveptr1 = NULL;
	cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
	subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
	if (subcmd1 == NULL) {
	    Serial.println(F("gps status - Show stat"));
	    Serial.println(F
			   ("gps shell - Direct commun with module"));
	} else if (strcmp_P(subcmd1, PSTR("status")) == 0) {
	    gps.f_get_position(&latitude, &longitude);
	    if (latitude == TinyGPS::GPS_INVALID_F_ANGLE
		|| longitude == TinyGPS::GPS_INVALID_F_ANGLE) {
		Serial.println(F("gps not ready"));
	    } else {
		Serial.print(F("gps_latitude="));
		Serial.println(latitude, 5);
		Serial.print(F("gps_longitude="));
		Serial.println(longitude, 5);
	    }
	} else if (strcmp_P(subcmd1, PSTR("shell")) == 0) {
            portForwarding(serialPortGPS, '\x03');
	}
    } else {
	Serial.print(F("Unknown cmd '"));
	Serial.print(cmd);
	Serial.println('\'');
    }
}

char serialBuf[40];
int serialBufLen = 0;
const unsigned long taskHandleSerialCommandInterval = 100;
unsigned long taskHandleSerialCommand(void *p)
{
    for (int i = 0; Serial.available() > 0 && i <= 40; i++) {
	int incomingByte = 0;
	incomingByte = Serial.read();
	if (incomingByte == '\r') {
	    /* run commands */
	    Serial.println();
	    serialBuf[sizeof(serialBuf) - 1] = '\0';
	    runCommand(serialBuf);
	    serialBufLen = 0;
	    serialBuf[0] = '\0';
	    Serial.print(F("lass-wonkt> "));
	    Serial.flush();
	} else if (incomingByte == '\n') {
	    /* ignore */
	} else {
	    if (serialBufLen + 1 <
		(sizeof(serialBuf) / sizeof(serialBuf[0]))) {
		serialBuf[serialBufLen] = incomingByte;
		serialBufLen++;
		serialBuf[serialBufLen] = '\0';
	    }
	    Serial.write(incomingByte);
	    Serial.flush();
	}
    }
    return taskHandleSerialCommandInterval;
}

const int numOfTasks = 8;
unsigned long taskPrevTime[numOfTasks] = {
    0, 0, 0, 0, 0, 0, 0, 0
};

unsigned long taskInterval[numOfTasks] = {
    taskHandleSerialCommandInterval,
    taskHandleLCDInterval,
    taskHandleDHTInterval,
    taskHandlePMSInterval,
    taskHandleGPSInterval,
    taskHandleESP_Status_Interval,
    taskHandleESP_NTP_Fail_Interval,
    taskHandleESP_MQTT_Interval
};

unsigned long (*tasks[numOfTasks])(void *) = {
    taskHandleSerialCommand,
    taskHandleLCD,
    taskHandleDHT,
    taskHandlePMS,
    taskHandleGPS,
    taskHandleESP_Status,
    taskHandleESP_NTP,
    taskHandleESP_MQTT
};

void loop()
{
    /* FCFS scheduling */
    unsigned long now = millis();
    int chosenTask = -1;
    unsigned long chosenTaskMaxWaitTime = 0;
    unsigned long minSleepTime = 1000;
    for (int i = 0; i < numOfTasks; i++) {
	unsigned long d = diffMS(taskPrevTime[i], now);
	if (d >= taskInterval[i]) {
	    if (chosenTask == -1
		|| (d - taskInterval[i]) > chosenTaskMaxWaitTime) {
		chosenTask = i;
		chosenTaskMaxWaitTime = d - taskInterval[i];
	    }
	} else {
	    if (taskInterval[i] - d < minSleepTime) {
		minSleepTime = taskInterval[i] - d;
	    }
	}
    }

    if (chosenTask != -1) {
	unsigned long nextTime = 0;
	nextTime = tasks[chosenTask] (NULL);
	taskPrevTime[chosenTask] = millis();
	taskInterval[chosenTask] = nextTime;
    } else {
	delay(minSleepTime);
    }
}
