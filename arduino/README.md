# LASS WonkT Arduino Nano firmware

## Libraries

In order to build this firmware, some libraries that must be installed.

* LiquidCrytal_I2C - https://github.com/lucasmaziero/LiquidCrystal_I2C
  - The library to drive the LCD
* PMS - https://github.com/fu-hsi/PMS
  - The library to drive PMS3003
* DHT-sensor-library - https://github.com/adafruit/DHT-sensor-library
  - The library to drive DHT11
* TinyGPS - https://github.com/mikalhart/TinyGPS
  - The library to drive GY-NEO6MV2
* MemoryFree - https://github.com/maniacbug/MemoryFree.git
  - The library to get free memory


